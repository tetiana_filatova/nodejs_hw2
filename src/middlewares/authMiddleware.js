const jwt = require('jsonwebtoken');

const {
  InvalidCredentialsError,
} = require('../utils/errors');

const authMiddleware = (req, res, next) => {
  const {
    authorization,
  } = req.headers;

  if (!authorization) {
    throw new InvalidCredentialsError('Please, provide "authorization" header');
  }

  const [, token] = authorization.split(' ');

  if (!token) {
    throw new InvalidCredentialsError('Please, include token to request');
  }

  try {
    const tokenPayload = jwt.verify(token, process.env.TOKEN);
    req.user = {
      userId: tokenPayload._id,
      username: tokenPayload.username,
    };
    next();
  } catch (err) {
    // res.status(401).json({message: err.message});
    throw new InvalidCredentialsError(err.message);
  }
};

module.exports = {
  authMiddleware,
};
