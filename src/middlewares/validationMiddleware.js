const Joi = require('joi');

const registrationValidator = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string()
        .alphanum()
        .min(3)
        .max(15)
        .required(),
    password: Joi.string()
        .min(6)
        .max(20)
        .required(),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    next(err);
  }
};

const loginValidator = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string()
        .required(),
    password: Joi.string()
        .required(),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    next(err);
  }
};

const noteValidator = async (req, res, next) => {
  const schema = Joi.object({
    text: Joi.string()
        .required(),
    completed: Joi.boolean()
        .optional(),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    next(err);
  }
};

module.exports = {
  registrationValidator,
  loginValidator,
  noteValidator,
};
