class NotesApiError extends Error {
  constructor(message) {
    super(message);
    this.status = 500;
  }
}

class InvalidRequestError extends NotesApiError {
  constructor(message = 'Invalid request') {
    super(message);
    this.status = 400;
  }
}

class InvalidCredentialsError extends NotesApiError {
  constructor(message = 'Not authorized') {
    super(message);
    this.status = 401;
  }
}

class InvalidPathError extends NotesApiError {
  constructor(message = 'Page not found') {
    super(message);
    this.status = 404;
  }
}

module.exports = {
  NotesApiError,
  InvalidRequestError,
  InvalidCredentialsError,
  InvalidPathError,
};
