const express = require('express');
const cookieParser = require("cookie-parser");
const router = new express.Router();

const {
  registration,
  logIn,
} = require('../services/authService');

const {
  asyncWrapper,
} = require('../utils/apiUtils');

const {
  registrationValidator,
  loginValidator,
} = require('../middlewares/validationMiddleware');

router.get('/register', asyncWrapper(async (req, res) => {
  res.render("index");
}));

router.post('/register', registrationValidator,
    asyncWrapper(async (req, res) => {
      const {
        username,
        password,
      } = req.body;

      await registration({username, password});

      res.json({message: 'Success'});
    }));

router.post('/login', loginValidator, asyncWrapper(async (req, res) => {
  const {
    username,
    password,
  } = req.body;

  const token = await logIn({username, password});
  res.cookie('authType', 'Bearer', {httpOnly: true});
  res.cookie('token', token, {httpOnly: true});
  res.json({message: 'Success', jwt_token: token});
}));

module.exports = {
  authRouter: router,
};
