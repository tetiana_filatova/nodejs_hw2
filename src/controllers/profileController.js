const express = require('express');
const router = new express.Router();

const {
  viewProfile,
  deleteProfile,
  changePassword,
} = require('../services/profileService');

const {
  asyncWrapper,
} = require('../utils/apiUtils');

router.get('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;

  const user = await viewProfile(userId);

  res.json({user});
}));

router.patch('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const oldPassword = req.body.oldPassword;
  const newPassword = req.body.newPassword;

  await changePassword(userId, oldPassword, newPassword);

  res.json({message: 'Success'});
}));

router.delete('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;

  await deleteProfile(userId);

  res.json({message: 'Success'});
}));

module.exports = {
  profileRouter: router,
};
