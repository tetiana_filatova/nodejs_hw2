const dotenv = require('dotenv');
dotenv.config();
const express = require('express');
const path = require('path');
const morgan = require('morgan');
const mongoose = require('mongoose');
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const app = express();

const port = process.env.PORT;
const {notesRouter} = require('./controllers/notesController');
const {authRouter} = require('./controllers/authController');
const {profileRouter} = require('./controllers/profileController');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {NotesApiError, InvalidPathError} = require('./utils/errors');

app.set("view engine", "ejs");
app.set('views', path.join(__dirname, 'views'));
app.use(express.static(path.join(__dirname, "public")));
app.use(bodyParser.urlencoded({ extended: false }));


app.use(express.json());
app.use(morgan('tiny'));
app.use(cookieParser());

app.use('/api/auth', authRouter);

// app.use(authMiddleware);
app.use('/api/notes', [authMiddleware], notesRouter);
app.use('/api/users/me', [authMiddleware], profileRouter);

app.use((req, res, next) => {
  throw new InvalidPathError('Not found');
});

app.use((err, req, res, next) => {
  if (err instanceof NotesApiError) {
    return res.status(err.status).json({message: err.message});
  }
  res.status(500).json({message: err.message});
});

const start = async () => {
  try {
    await mongoose.connect(process.env.DB_URI, {
      useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true
    });

    app.listen(port);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
