const {User} = require('../models/userModel');
const bcrypt = require('bcrypt');

const viewProfile = async (userId) => {
  const user = await User.findById({_id: userId}, '-password -__v');
  return user;
};

const deleteProfile = async (userId) => {
  await User.findOneAndRemove({_id: userId});
};

const changePassword = async (userId, oldPassword, newPassword) => {
  const user = await User.findOne({_id: userId});

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    throw new Error('Invalid current password');
  }

  await User.updateOne({_id: userId}, {$set: {password:
    await bcrypt.hash(newPassword, 10)}});
};

module.exports = {
  viewProfile,
  deleteProfile,
  changePassword,
};
